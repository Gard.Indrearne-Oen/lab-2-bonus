package INF101.lab2;

import java.util.ArrayList;

public class Fridge implements IFridge{
    int totalSize = 20;
    int nItemsInFridge;
    ArrayList<FridgeItem> itemsInFridge = new ArrayList<>();
    
    public int nItemsInFridge() {
        return this.nItemsInFridge;
    }

    public int totalSize() {
        return this.totalSize;
    }

    public boolean placeIn(FridgeItem item) {
        return this.nItemsInFridge != this.totalSize;
    }

    public void takeOut(FridgeItem item) {
        item.getName();

    }
    
    public void emptyFridge() {

    }
    public List<FridgeItem> removeExpiredFood() {
        return List<FridgeItem>;

    }

}
